import time

import requests

apple_num = 0
delay = 1


if __name__=='__main__':

    url = 'http://192.168.0.21:8000/apple-packer/'

    while True:

        time.sleep(delay)
        distance = requests.get(url + 'read-buffer').json()     # 1초 간격으로 센서값 요청
        if distance < 8:
            requests.post(url + 'load')                         # 센서앞 사과가 있다면 사과 적재
            time.sleep(delay)

            res_weight = requests.get(url + 'weight')
            if res_weight.status_code == 200:               # request 요청 성공 유무
                if res_weight.json()['weight'] != 0:        # 사과 접시위의 무게가 0이 아니라면
                    print(res_weight.json())
                    time.sleep(delay)

                    res_send_to_packer = requests.post(url + 'send-to-packer')      # 사과 접시에서 포장기로 보냄
                    time.sleep(delay)

                    apple_num += 1
                    print('현재까지 공급된 사과 개수: ', apple_num)
