from fastapi import FastAPI, HTTPException
import uvicorn
import threading
import time

import keyboard
from motorController import MotorController
from weightController import WeightController
from tiltController import TiltController

app = FastAPI(title="사과 공급 제어 서버",
              description="사과 공급의 단계별 제어를 수행하는 서버")

mc = MotorController('COM10')
wc = WeightController('COM11')
tc = TiltController('COM12')

@app.post("/apple-packer/load")
def load():

    mc.drop()


@app.get("/apple-packer/weight")
def get_weight():

    while True:
        wc.readBuffer()
        if wc.status == 'ST':
            data = {
                'status': wc.status,
                'weight': wc.weight,
            }
            break

    return data


@app.post("/apple-packer/send-to-packer")
def send_to_packer():

    tc.tilt()
    time.sleep(1)
    tc.ready()

@app.get("/apple-packer/read-buffer")
def read_buffer():

    distance = mc.distance

    return distance

def run_server():
    uvicorn.run(app, host='0.0.0.0', port=8000)


if __name__=='__main__':
    keyboard.add_hotkey('3', mc.run_motor1)
    keyboard.add_hotkey('1', mc.stop_motor1)

    keyboard.add_hotkey('4', tc.tilt)
    keyboard.add_hotkey('6', tc.ready)

    keyboard.add_hotkey('9', mc.run_motor2)
    keyboard.add_hotkey('7', mc.stop_motor2)

    server_thread = threading.Thread(target=run_server)
    server_thread.start()

    motor_thread = threading.Thread(target=mc.main)
    motor_thread.start()


