import threading
import time

import serial

class WeightController:

    def __init__(self, serial_port='COM11'):
        self.status = ''                        # ST(stable) or US(unstable)
        self.sign = ''                         # 무게 부호 : + or -
        self.weight = 0
        self.lock = threading.Lock()
        self.serial_io = serial.Serial(serial_port, baudrate=9600, parity='N', stopbits=1, bytesize=8, timeout=8)

    def readBuffer(self):
        self.serial_io.flushInput()  # 이전에 쌓인 버퍼 지우기
        content = self.serial_io.readline()
        self.status = content[0:2].decode()
        self.sign = content[3:4].decode()
        self.weight = float(content[4:-5].decode())
        # print(content)
        # print(self.status, self.sign, self.weight)

    def main(self):
        while True:
            self.readBuffer()