import threading
import time

import serial

class TiltController:

    def __init__(self, serial_port='COM12'):
        self.status = 'ready'
        self.serial_io = serial.Serial(serial_port, baudrate=115200, parity='N', stopbits=1, bytesize=8, timeout=8)

    def tilt(self):
        command = 'tilt'
        self.serial_io.write(command.encode())
        self.status = 'tilt'

    def ready(self):
        command = 'ready'
        self.serial_io.write(command.encode())
        self.status = 'ready'