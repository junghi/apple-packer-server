import threading
import time

import keyboard

from motorController import MotorController
from weightController import WeightController
from tiltController import TiltController


mc = MotorController('COM10')
wc = WeightController('COM11')
tc = TiltController('COM12')


if __name__ == "__main__":
    keyboard.add_hotkey('3', mc.run_motor1)
    keyboard.add_hotkey('1', mc.stop_motor1)

    keyboard.add_hotkey('4', tc.tilt)
    keyboard.add_hotkey('6', tc.ready)

    while True:
        mc.readBuffer()

        if mc.distance < 10 and mc.is_on == True:
            mc.stop_motor1()
            time.sleep(1)

            if tc.status == 'ready':
                mc.drop()
                time.sleep(1.5)
                print(wc.weight)
                tc.tilt()
                time.sleep(1)
                tc.ready()
                mc.run_motor1()


