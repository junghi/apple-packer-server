import threading
import time

import serial
import keyboard

class MotorController:

    def __init__(self, serial_port='COM10'):
        self.lock = threading.Lock()
        self.is_on = True                                           # 컨베이어 벨트 작동 유무
        self.distance = 20
        self.lock = threading.Lock()
        self.serial_io = serial.Serial(serial_port, baudrate=115200, parity='N', stopbits=1, bytesize=8, timeout=8)

        # command = 'off 1'
        # self.serial_io.write(command.encode())

    def readBuffer(self):
        try:
            self.serial_io.flushInput()                             #이전에 쌓인 버퍼 지우기
            content = self.serial_io.readline()
            self.distance = float(content[:-2].decode())
            # print("센서 인식 거리 : ", self.distance, self.is_on)
        except Exception as e:
            print(e, content)

    def run_motor1(self):
        command = 'off 1'
        self.serial_io.write(command.encode())
        self.is_on = True

    def stop_motor1(self):
        command = 'on 1'
        self.serial_io.write(command.encode())
        self.is_on = False

    def run_motor2(self):
        command = 'off 2'
        self.serial_io.write(command.encode())

    def stop_motor2(self):
        command = 'on 2'
        self.serial_io.write(command.encode())

    def drop(self):
        command = 'off 1'
        self.serial_io.write(command.encode())
        time.sleep(1.1)                                               # 센서 앞 사과가 사라질 때까지 강제 컨베이어 run시킴
        self.is_on = True
        # command = 'on 1'
        # self.serial_io.write(command.encode())

    def main(self):
        while True:
            self.readBuffer()
            if self.distance < 8 and self.is_on == True:       # 센서 앞에 사과가 있고, 컨베이어가 작동 중 이라면
                self.stop_motor1()



